﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinnTask.Models
{
    public class Consignment
    {
        public int ConsignmentId { get; set; }
        public string ConsignmentDate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public int PhoneNumber { get; set; }
        public string CountryISO2 { get; set; }
        public string PostalCode { get; set; }
        public decimal PackageWidth { get; set; }
        public decimal PackageHeight { get; set; }
        public string PackageDepth { get; set; }
        public string PackageType { get; set; }
        public string ItemCode { get; set; }
        public string Quantity { get; set; }
        public decimal UnitWeight { get; set; }
    }
}
