﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsignmentApplication.Models
{
    public class Item
    {
        public int ItemId { get; set; }
        public int PackageId { get; set; }
        public string ItemCode { get; set; }
        public string Quantity { get; set; }
        public decimal UnitWeight { get; set; }
    }
}
