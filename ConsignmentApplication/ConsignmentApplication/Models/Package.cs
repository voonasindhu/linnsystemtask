﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsignmentApplication.Models
{
    public class Package
    {
        public int PackageId { get; set; }
        public int ConsignmentId { get; set; }
        public decimal PackageWidth { get; set; }
        public decimal PackageHeight { get; set; }
        public string PackageDepth { get; set; }
        public string PackageType { get; set; }
        public List<Item> Items { get; set; }
    }
}
