﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using LinnTask.Models;
using System.Data.SqlClient;
using System.Configuration;


namespace LinnTask
{
    public class Program
    {
       static void Main(string[] args)
        {
            //initializing the connection string to connect the Database
            var da = new DataAdapter();

            try
            {
                //Add the Consignment, Package and Items list to the database and retrieve the ConsingmentId 
                //ConsignmentId is returned in order to get the saved consignment details
                var consignmentId = da.Add();

                //Create new instance of Consignment
                var _consignment = new List<Consignment>();
                if (consignmentId > 0)
                {
                    //passing consignmentId to retrieve consignment details from database
                    DataTable dt = da.Get(consignmentId);

                    // If - Else Statement 
                    if (dt.Rows.Count > 0) //check the data whether it is existing or not
                    {
                        // retrieve items from List of collection by using foreach loop
                        foreach (DataRow dr in dt.Rows)
                        {
                            //Mapping the retrieve data into _consignment declaration variable
                            var res = new Consignment
                            {
                                ConsignmentDate = string.Format("{0:d}", dr["ConsignmentDate"]),
                                Address1 = dr["Address1"].ToString(),
                                Address2 = dr["Address2"].ToString(),
                                Address3 = dr["Address3"].ToString(),
                                City = dr["City"].ToString(),
                                PhoneNumber = int.Parse(dr["PhoneNumber"].ToString()),
                                CountryISO2 = dr["CountryISO2"].ToString(),
                                PostalCode = dr["PostalCode"].ToString(),
                                PackageWidth = decimal.Parse(dr["PackageWidth"].ToString()),
                                PackageHeight = decimal.Parse(dr["PackageHeight"].ToString()),
                                PackageDepth = dr["PackageDepth"].ToString(),
                                PackageType = dr["PackageType"].ToString(),
                                ItemCode = dr["ItemCode"].ToString(),
                                Quantity = dr["Quantity"].ToString(),
                                UnitWeight = decimal.Parse(dr["UnitWeight"].ToString()),
                            };
                            _consignment.Add(res);

                            //TODO print the values of the res
                            Console.WriteLine(res.ConsignmentDate + " |" + res.Address1 + " |" + res.Address2 + " |" + res.Address3 + " |"
                                + res.City + " |" + res.PhoneNumber + " |" + res.CountryISO2 + " | " + res.PostalCode + " |" + res.PackageWidth 
                                + " |" + res.PackageHeight + " |" + res.PackageDepth + " |" + res.PackageType + " |" + res.ItemCode + " |"
                                + res.Quantity + " |" + res.UnitWeight + "\n");
                        }
                    }
                    else //If there are no rows it displays no rows found 
                    {
                        Console.WriteLine("No rows found");
                    }
                }
                else // If there is no data it displays this message
                {
                    Console.WriteLine("No data found");
                }
            }
            catch(Exception ex) // Throw an exception
            {
                Console.WriteLine(ex.Message); //Error handles the code and display the error message
            }
            Console.ReadLine();
        }
       
    }
}