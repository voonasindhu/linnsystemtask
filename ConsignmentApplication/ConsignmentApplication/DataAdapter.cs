﻿using System;
using LinnTask.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;

namespace LinnTask
{
    public class DataAdapter : IDisposable
    {
        SqlConnection sqlConn;
        SqlCommand command;
        SqlDataReader dtaReader;
        SqlDataAdapter adapter;

        //  Create the connectionString
        public DataAdapter()
        {
            // Gets the connection string from web.config file and initialises connection with database 
            sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlConnector"].ConnectionString);
            // Open the connection
            sqlConn.Open();
        }

        public void addSP(string storedProc)
        {
            clearObjects();

            // create a command object identifying the stored procedure
            command = new SqlCommand(storedProc, sqlConn);

            // set the command object so it knows to execute a stored procedure
            command.CommandType = CommandType.StoredProcedure;
        }

        public void addTableAsParameter(string name, SqlDbType type, DataTable value)
        {
            //add parameter to command, which will be passed to the stored procedure
            SqlParameter sqlParam = new SqlParameter(name, type);
            sqlParam.Direction = ParameterDirection.Input;
            sqlParam.Value = value;
            command.Parameters.Add(sqlParam);
        }

        public void addparameter(string name, SqlDbType type)
        {
            //add output parameter to command, which will be retrieve to the stored procedure
            SqlParameter sqlParam = new SqlParameter(name, type);
            sqlParam.Direction = ParameterDirection.Output;
            command.Parameters.Add(sqlParam).Direction = sqlParam.Direction;
        }

        public void addParameter(string name, SqlDbType type, int value)
        {
            // add parameter to command, which will be passed to the stored procedure
            SqlParameter sqlParam = new SqlParameter(name, type);
            sqlParam.Direction = ParameterDirection.Input;
            sqlParam.Value = value;
            command.Parameters.Add(sqlParam);
        }

        // Execute the stored procedure
        public bool executeSP(out int consignmentId, string name)
        {
            // check the connectivity issues and dead locks
            bool returnValue = false;
            var deadlock = false;
            int cnt = 0;
            consignmentId = 0;
            //tries to re connect to the sql server database for atleast 10 times if there is either deadlock or timeout
            do
            {
                try
                {
                    //Executes the query
                    command.ExecuteNonQuery();
                    //Retrieve the output value from strored procedure
                    consignmentId = (int)command.Parameters[name].Value;
                    deadlock = false;
                }
                catch (SqlException ex) //throw an exception
                {
                    //  check the connectivity issues and dead locks
                    //error handles the code, if exception number is 1205 then its a deadlock or it is a timeout issue if the exception message contains "Timeout" string
                    // and sets the deadlock variable to true
                    if (ex.Number == 1205 || ex.Message.Contains("Timeout"))
                    {
                        deadlock = true;
                    }
                }
                catch(Exception ex)
                {
                    //if there is an exception other than timeout or deadlock returns false;
                    deadlock = false;
                    returnValue = false;
                }
                cnt++;
                //if the counter value is less than 10 and deadlock or timeout issues didn't occured returns true
                if (cnt < 10 && !deadlock) returnValue = true;
            } while (deadlock && cnt < 10); //runs the loop for the max of 10 times if there is either connection timeout issue or a deadlock
            return returnValue;
        }

        // Execute the stored procedure and retrieve the data from database
        public DataTable executeSPwithTableQuery()
        {
            var dt = new DataTable();
            var deadlock = false;
            int cntr = 0;
            //tries to re connect to the sql server database for atleast 10 times if there is either deadlock or timeout
            do
            {
                try
                {
                    adapter = new SqlDataAdapter(command);
                    //Create new instance of DataSet
                    var ds = new DataSet();
                    adapter.Fill(ds);
                    //  fill the retrieved Dataset tables into DataTale
                    dt = ds.Tables[0];
                    deadlock = false;
                }
                catch (SqlException ex) //throw an exception
                {
                    //  check the connectivity issues and dead locks
                    //error handles the code, if exception number is 1205 then its a deadlock or it is a timeout issue if the exception message contains "Timeout" string
                    // and sets the deadlock variable to true
                    if (ex.Number == 1205 || ex.Message.Contains("Timeout")) deadlock = true;
                }
                catch(Exception ex)
                {
                    //if there is an exception other than timeout or deadlock just returns with empty datatable.
                    deadlock = false;
                    dt = null;
                }
                cntr++;

            } while (deadlock && cntr < 10);//runs the loop for the max of 10 times if there is either connection timeout issue or a deadlock            
            return dt;
        }

        // clear the objects 
        public void clearObjects()
        {
            // check the SqlDataReader is null or not 
            if (dtaReader != null)
            {
                //  close the SqlDataReader
                if (!dtaReader.IsClosed) { dtaReader.Close(); };
                dtaReader = null;
            }

            // check the SqlCommand is null or not 
            if (command != null)
            {
                // Dispose the SqlCommand
                command.Dispose();
                command = null;
            };
        }

        //  Protected implementation of Dispose pattern.
        public void Dispose()
        {
            clearObjects();

            // checks the sqlconnection wheather it is null or not
            if (sqlConn != null)
            {
                //Closes sql connection if it is still open
                if (sqlConn.State != System.Data.ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
                sqlConn.Dispose();
            }
        }

        public static DataTable CreateConsignment()
        {
           // Create Consignment Datatable
            var consignmentdt = new DataTable();
            consignmentdt.Columns.Add("Address1", typeof(string));
            consignmentdt.Columns.Add("Address2", typeof(string));
            consignmentdt.Columns.Add("Address3", typeof(string));
            consignmentdt.Columns.Add("City", typeof(string));
            consignmentdt.Columns.Add("PhoneNumber", typeof(string));
            consignmentdt.Columns.Add("CountryISO2", typeof(string));
            consignmentdt.Columns.Add("PostalCode", typeof(string));

            DataRow dr;
            // create Datarows for Consignment
            dr = consignmentdt.NewRow();
            dr["Address1"] = "8";
            dr["Address2"] = "Barnes Wallis Road";
            dr["Address3"] = "Arena";
            dr["City"] = "Fareham";
            dr["PhoneNumber"] = "0123456789";
            dr["CountryISO2"] = "123456";
            dr["PostalCode"] = "AB1 2EF";
            //Add the row to the table
            consignmentdt.Rows.Add(dr);
            return consignmentdt;
        }

        public static DataTable CreatePackage()
        {
            //Create Package Datatable and columns to it 
            var packagedt = new DataTable();
            packagedt.Columns.Add("PackageWidth", typeof(decimal));
            packagedt.Columns.Add("PackageHeight", typeof(decimal));
            packagedt.Columns.Add("PackageDepth", typeof(string));
            packagedt.Columns.Add("PackageType", typeof(string));

            DataRow dr;
            // create Datarows for Package
            dr = packagedt.NewRow();
            dr["PackageWidth"] = 2.5;
            dr["PackageHeight"] = 2.5;
            dr["PackageDepth"] = "ABCD";
            dr["PackageType"] = "ABCD";
            //Add the row to the table
            packagedt.Rows.Add(dr);

            return packagedt;
        }

        public static DataTable CreateItems()
        {
            //Create Item Datatable and columns to it 
            
            var itemdt = new DataTable();
            itemdt.Columns.Add("ItemCode", typeof(string));
            itemdt.Columns.Add("Quantity", typeof(string));
            itemdt.Columns.Add("UnitWeight", typeof(double));
            for (var i = 0; i < 2; i++)
            {
                DataRow dr;
                // create Datarows for Items
                dr = itemdt.NewRow();
                dr["ItemCode"] = "Tester" + i;
                dr["Quantity"] = i.ToString();
                dr["UnitWeight"] = Convert.ToDouble(i * 10 + "0.00");
                //Add the row to the table
                itemdt.Rows.Add(dr);
            }
            return itemdt;
        }

        public int Add()
        {
            int consignmentId;
            // Intilizing the Datatable into Declaration variable
            
            var _Consignmentdt = CreateConsignment();
            var _packagedt = CreatePackage();
            var _itemdt = CreateItems();

            //initializing the connection string to connect the Database
            using (var dbCon = new DataAdapter())
            {
                // create a command object identifying the stored procedure
                dbCon.addSP("spAddConsignment");

                //  add table parameter to command, which will be passed to the stored procedure
                //used table-valued parameters to send multiple rows of data to a stored procedure, without creating a temporary table or many parameters.
                dbCon.addTableAsParameter("@consignemnttable", SqlDbType.Structured, _Consignmentdt);
                dbCon.addTableAsParameter("@packagetable", SqlDbType.Structured, _packagedt);
                dbCon.addTableAsParameter("@itemtable", SqlDbType.Structured, _itemdt);
                //the below function creates output paramter to get the consigment id 
                dbCon.addparameter("@Id", SqlDbType.Int);

                // calls the executeSp function to execute the query qnd retrun the output value          
                dbCon.executeSP(out consignmentId, "@Id");  
            }

            // return the Consignment Id 
            return consignmentId;
        }

        public DataTable Get(int id)
        {
            var dt = new DataTable(); //int

            //initializing the connection string to connect the Database
            using (var dbcon = new DataAdapter())
            {
                // create a command object identifying the stored procedure
                dbcon.addSP("spGetConsignment");

                // add parameter to command, which will be passed to the stored procedure
                dbcon.addParameter("@Id", SqlDbType.Int, id);

                // call the executeSPwithTableQuery function to Execute the query and retrieve the data from database
                dt = dbcon.executeSPwithTableQuery();
            }
            return dt;
        }
    }
}